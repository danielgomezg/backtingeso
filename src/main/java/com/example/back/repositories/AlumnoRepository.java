package com.example.back.repositories;

import com.example.back.models.Alumno;
import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface AlumnoRepository extends JpaRepository<Alumno, Integer>{
    Alumno findAlumnoById(Integer id);
    Alumno findAlumnoByNombre(String name);
}