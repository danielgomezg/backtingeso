package com.example.back.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.*;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import com.example.back.repositories.AlumnoRepository;
import com.example.back.models.Alumno;

@RestController
@RequestMapping("/alumno")
@CrossOrigin(origins = "*")
public class AlumnoController{

    @Autowired
    AlumnoRepository repository;

    //obtener alumno
    @GetMapping("")
    public ResponseEntity<List<Alumno>> getAllAlumnos(){
        try {
            List<Alumno> students = new ArrayList<Alumno>();
            repository.findAll().forEach(students::add);
            if(students.isEmpty()){
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }
            return new ResponseEntity<>(students, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(null,HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    //insertar alumno
    @PostMapping("/nuevo")
    public Alumno storeAlumno(@RequestBody Alumno newAlumno){
        return repository.save(newAlumno);
    }
    

    //obtener un producto
    @PutMapping("/{id}")
    public int updateAlumno(@RequestBody Alumno updateAlumno, @PathVariable Integer id){
        repository.findById(id)
                .map(Alumno -> {
                    Alumno.setNombre(updateAlumno.getNombre());
                    Alumno.setRut(updateAlumno.getRut());
                    Alumno.setFechaNacimiento(updateAlumno.getFechaNacimiento());
                    Alumno.setCarrera(updateAlumno.getCarrera());
                    repository.save(Alumno);
                    return 0;
                });
        return 0;
    }

    //Eliminiar
    @DeleteMapping("/{id}")
    public void deleteAlumno(@PathVariable Integer id){
        repository.deleteById(id);
    }
}