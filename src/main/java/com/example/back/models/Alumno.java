package com.example.back.models;

import lombok.Data;
import lombok.NonNull;
import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "alumno")
public class Alumno implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", updatable = false, nullable = false)
    private Integer id;
    @NonNull
    @Column(name = "nombre", updatable = false, nullable = false)
    private String nombre;
    @NonNull
    @Column(name = "rut", updatable = false, nullable = false)
    private String rut;
    @NonNull
    @Column(name = "fechaNacimiento", updatable = false, nullable = false)
    private Date fechaNacimiento;
    @NonNull
    @Column(name = "Carrera", updatable = false, nullable = false)
    private String carrera;

    public Alumno(String nombre, String rut, String carrera, Date date){
        this.rut =rut;
        this.nombre = nombre;
        this.carrera = carrera;
        this.fechaNacimiento = date;
    }

    public int getId(){
        return id;
    }

    public void setId(int id){
        this.id = id;
    }

    public String getNombre(){
        return nombre;
    }

    public void setNombre(String nombre){
        this.nombre = nombre;
    }

    public String getRut(){
        return rut;
    }

    public void setRut(String rut){
        this.rut = rut;
    }

    public Date getFechaNacimiento(){
        return fechaNacimiento;
    }

    public void setFechaNacimiento(Date fechaNacimiento){
        this.fechaNacimiento = fechaNacimiento;
    }

    public String getCarrera(){
        return carrera;
    }

    public void setCarrera(String carrera){
        this.carrera = carrera;
    }

}


